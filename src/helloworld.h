// Copyright (c) 2021 Helmholtz-Zentrum Dresden - Rossendorf e.V.
//
// SPDX-License-Identifier: MIT

#ifndef SRC_HELLOWORLD_H_
#define SRC_HELLOWORLD_H_

#include <string>

// Return content of the variable GITLAB_USER_NAME
std::string getUserNameViaEnv();

#endif  // SRC_HELLOWORLD_H_
